# Sikuyuf: a simplistic osu! STD skin with customization only for gameplay, along with customized osu!lazer layout

## Introduction
`Sikuyuf` is an simplistic osu! skin which only includes necessary customization in osu! STD gameplay but the UI, and has customization for the gameplay layout of osu!lazer. It has 4 different styles named `clear`, `clear-instafade`, `crystal` and `ring`, which differ from each other mainly by hitcircles.

You can remix any skin's UI into this by unzipping `Sikuyuf-xxx.osk` into the skin's folder and replacing all conflicting files. Feel free to remix (any part of) this skin into yours.

Most contents are drawn by `fuyukiS` with [Inkscape](https://inkscape.org/). The font is `Torus Pro`, the same font used by osu!lazer. The HP bar is from osu! default skin. Hitsounds are from `Prawilosc vJP without followpoints` by `DDK RPK`.

## Download

see [Releases](https://gitlab.com/fuyukiS/sikuyuf-the-osu-skin/-/releases)

## Screenshots

All of the below are shot in osu!lazer during replays.

#### Sikuyuf-clear
![](screenshots/clear0.png)
![](screenshots/clear1.png)
![](screenshots/clear2.png)

#### Sikuyuf-ring
![](screenshots/ring0.png)
![](screenshots/ring1.png)

#### Sikuyuf-crystal
![](screenshots/crystal0.png)
![](screenshots/crystal1.png)

#### Sikuyuf-clear-instafade
![](screenshots/instafade0.png)
![](screenshots/instafade1.png)

#### Spinners
![](screenshots/spinner0.png)
![](screenshots/spinner1.png)

## Customization (optional)
First of all, if you run osu!lazer and want to customize this skin, you have to unzip the `Sikuyuf-xxx.osk`, customize it, compress all the files into a zip file, change its suffix name (`.zip`) into `.osk` BEFORE installing it into the game since osu!lazer has a different way to store files. If you run osu!stable you can install the skin first and modify it in the skin folder.

#### Simplify the Cursor
If you don't like the rotating decoration around the cursor, replace `cursor.png` with a 1x1 transparent image and delete `cursor@2x.png`.

#### Classical Cursor Trail
If you want the old style of the cursor trail, delete `cursor.png` and `cursor@2x.png` (or merging them into `cursormiddle(@2x).png` if you want to keep the decoration), then rename `cursormiddle(@2x).png` to `cursor(@2x).png`, then modify `skin.ini` as below (unless you keep the decoration):
```
...
CursorRotate: 1    ->  CursorRotate: 0
CursorExpand: 1    ->  CursorExpand: 0
...
```
After that replace `cursortrail(@2x).png` with the ones in `Extra.zip`.

#### Replace Numbers inside hitcircles with Dots
If you want dots instead of numbers in your hitcircles (just like `Sikuyuf-clear-instafade`), copy all the `hcdot-*(@2x).png` in `Extra.zip` into your folder, then modify `skin.ini` as below:
```
...
[Fonts]
HitCirclePrefix: default    ->  HitCirclePrefix: hcdot
HitCircleOverlap: 11        ->  HitCircleOverlap: 40
...
```

#### Instafade
If you want instafade (hitcircles fading instantly without animations after being hit) but not `clear-instafade`, follow [this guide](https://skinship.xyz/guides/insta_fade_hc) or use [this tool](https://github.com/clarks03/Osu-Instafade-Tool). You can find dots for hitcircles in `Extra.zip`. When scaling the hitcircles, `hitcircle.svg` and `hitcircleoverlay.svg` in [the repository](https://gitlab.com/fuyukiS/sikuyuf-the-osu-skin) are recommended since they are vector and can be scaled at any ratio.
